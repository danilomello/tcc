package LoginSigaOR;

import LoginSigaOR.Page.LoginPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.WebDriver;
import suporte.Web;

@RunWith(JUnit4.class)
public class LoginTest {

    private WebDriver navegador;

    @Before
    public void setup() {
        navegador = Web.createChrome("https://siga.cps.sp.gov.br/aluno/login.aspx?");
    }

    @Test
    public void realizarLogin() {
        String nomeAluno = new LoginPage(navegador)
                .realizarLogin("346641780", "DaniloCGM=25")
                .capturarNome();
        Assert.assertEquals("NOME DO ALUNO", nomeAluno);
    }

    @After
    public void tearDown() {
        navegador.close();
    }
}
