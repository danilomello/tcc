package LoginSigaOR.ObjectRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPageRepository {

    @FindBy(how = How.ID, using = "vSIS_USUARIOID")
    public static WebElement campoEmail;

    @FindBy(how = How.ID, using = "vSIS_USUARIOSENHA")
    public static WebElement campoSenha;

    @FindBy(how = How.NAME, using = "BTCONFIRMA")
    public static WebElement botaoConfirma;

}
