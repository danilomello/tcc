package LoginSigaOR.Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PaginaInicial extends BasePage {

    public PaginaInicial(WebDriver navegador) {
        super(navegador);
    }

    public String capturarNome(){
        return navegador.findElement(By.id("span_MPW0041vPRO_PESSOALNOME")).getText();
    }

}
