package LoginSigaOR.Page;

import LoginSigaOR.ObjectRepository.LoginPageRepository;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static LoginSigaOR.ObjectRepository.LoginPageRepository.*;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver navegador){
        super(navegador);
        PageFactory.initElements(navegador, LoginPageRepository.class);
    }

    public PaginaInicial realizarLogin(String usuario, String senha){
        WebDriverWait wait = new WebDriverWait(navegador, 10L);
        wait.until(ExpectedConditions.elementToBeClickable(botaoConfirma));
        campoEmail.sendKeys(usuario);
        campoSenha.sendKeys(senha);
        botaoConfirma.click();
        return new PaginaInicial(navegador);
    }

}
