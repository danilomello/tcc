package ByChained;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ByChained;
import suporte.Web;

@RunWith(JUnit4.class)
public class TesteByChained {

    private WebDriver navegador;

    @Before
    public void setup(){
        navegador = Web.createChrome();
    }

    @Test
    public void testeComByChained(){
        By myBy = new ByChained(By.className("topicos"), By.className("col_esq"),By.className("campo_check"));
        String texto = navegador.findElements(myBy).get(0).getText();
        Assert.assertEquals("2ª via de Conta", texto);
    }

    @Test @Ignore
    public void testeComIdentificadorSimples(){
        WebElement elemento = navegador.findElements(By.className("topicos")).get(1);
        elemento = elemento.findElement(By.className("col_esq"));
        String texto = elemento.findElements(By.className("campo_check")).get(0).getText();
        Assert.assertEquals("2ª via de Conta", texto);
    }

    @After
    public void teardown(){
        navegador.quit();
    }

}
