package LoginSigaPO.Page;

import LoginSigaOR.ObjectRepository.PaginaInicialRepository;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import static LoginSigaOR.ObjectRepository.PaginaInicialRepository.nomeAluno;


public class PaginaInicial extends BasePage {

    public PaginaInicial(WebDriver navegador) {
        super(navegador);
        PageFactory.initElements(navegador, PaginaInicialRepository.class);
    }

    public String capturarNome(){
        return nomeAluno.getText();
    }

}
