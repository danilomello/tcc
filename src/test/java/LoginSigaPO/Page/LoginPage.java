package LoginSigaPO.Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver navegador){
        super(navegador);
    }

    public PaginaInicial realizarLogin(String usuario, String senha){
        navegador.findElement(By.id("vSIS_USUARIOID")).sendKeys(usuario);
        navegador.findElement(By.id("vSIS_USUARIOSENHA")).sendKeys(senha);
        navegador.findElement(By.name("BTCONFIRMA")).click();
        return new PaginaInicial(navegador);
    }

}
