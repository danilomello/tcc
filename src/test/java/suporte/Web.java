package suporte;

import com.google.common.io.Resources;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Web {

    private static WebDriver navegador;

    public static WebDriver createChrome(){

        String driverUrl = Resources.getResource("chromedriver.exe").getPath();
        String url = Resources.getResource("paginateste3.html").getPath().toString();

        return createChrome(driverUrl, url);
    }

    public static WebDriver createChrome(String url){
        return createChrome(null, url);
    }

    public static WebDriver createChrome(String driverPath, String url) {

        if (driverPath == null)
            driverPath = Resources.getResource("chromedriver.exe").getPath();
        System.setProperty("webdriver.chrome.driver", driverPath);
        navegador = new ChromeDriver();
        setImplicitWait();
        navegador.get(url);
        return navegador;
    }

    public static void setImplicitWait(){
        navegador.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }
}
