package LoginSiga;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import suporte.Web;

@RunWith(JUnit4.class)
public class LoginTest {

    private WebDriver navegador;

    @Before
    public void setup() {
        navegador = Web.createChrome("https://siga.cps.sp.gov.br/aluno/login.aspx?");
    }

    @Test
    public void realizarLoginComSucesso() {
        WebElement campoUsuario = navegador.findElement(By.id("vSIS_USUARIOID"));
        WebElement campoSenha = navegador.findElement(By.id("vSIS_USUARIOSENHA"));
        WebElement botaoConfirma = navegador.findElement(By.name("BTCONFIRMA"));

        WebDriverWait wait = new WebDriverWait(navegador, 10L);
        wait.until(ExpectedConditions.elementToBeClickable(botaoConfirma));

        campoUsuario.sendKeys("USUARIO");
        campoSenha.sendKeys("SENHA");
        botaoConfirma.click();

        Assert.assertEquals("NOME DO ALUNO",
                navegador.findElement(By.id("span_MPW0041vPRO_PESSOALNOME")).getText());


    }

    @After
    public void tearDown() {
        navegador.close();
    }
}
